/*
 * Copyright 2017-2018, Joachim Kuebart <joachim.kuebart@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *      1. Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *
 *      2. Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the
 *         distribution.
 *
 *      3. Neither the name of the copyright holder nor the names of its
 *         contributors may be used to endorse or promote products derived
 *         from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*property
    argv, code, column, edition, error, exit, forEach, freeze, fudge, g, h,
    indexOf, lastIndexOf, length, line, log, map, message, next, o, ok, optarg,
    optind, p, parallel, parse, property, push, realpath, reduce, sequence,
    slice, some, stop, substr, substring, v, warnings
*/

import {constant, readFile, requestorize, parseqToNode} from "./util.js";

import jslint from "@jkuebart/jslint";
import jslintReport from "@jkuebart/jslint/dist/report.js";
import parseq from "@jkuebart/parseq";
import Getopt from "classic-getopt";

import console from "console";
import fs from "fs";
import process from "process";

// Sum an array of values.
const sum = (ary) => ary.reduce((sum, item) => sum + item, 0);

// Compute the canonical pathname.
const realpath = (callback, path) => fs.realpath(
    path,
    "utf8",
    parseqToNode(callback)
);

// Parse an option and add it to the option_object.
function parseOption(option_object, optarg) {
    let option = optarg;
    let value = true;
    const eq = option.indexOf("=");
    if (-1 !== eq) {
        option = option.substr(0, eq);
        const json_value = optarg.substr(1 + eq);
        try {
            value = JSON.parse(json_value);
        } catch (exc) {
            console.error(
                `Invalid value for option "${optarg.substr(0, eq)}": ` +
                `"${json_value}": ${exc}.`
            );
            return false;
        }
    }
    option_object[option] = value;
    return true;
}

// Determine whether a report has any warnings of the given type.
const hasWarning = (report, code) => report.warnings.some(
    (warn) => warn.code === code
);

const basename = (name) => name.substring(1 + name.lastIndexOf("/"));

function usage() {
    console.log(`
usage: ${basename(process.argv[1])} [-hpv][-g <global>][-o <option>[=<value>]]
            [files...]

    -g <global>

        The name of a global variable that the file is allowed readonly
        access. Can be used multiple times. Also see -o for options which
        automatically define global variables for well-known environments
        such as browser or Node.js.

    -h

        Print usage instructions and exit.

    -o <option>[=<value>]

        Pass an option to JSLint. Check the JSLint documentation for the
        full list of options. The option is set to true if no value is
        given, otherwise it should be a valid JSON string.

    -p

        Print a /*property directive which can be pasted into the source
        file. By default, the directive is only printed if there are
        warnings about unregistered properties.

    -v

        Print the »edition« of the bundled JSLint and exit.
`);
}

function main(argv) {
    // Parse command line options
    const parser = new Getopt(argv, "g:ho:pv");

    const opt = {
        "g": [],
        "h": false,
        "o": {},
        "p": false,
        "v": false
    };

    let option = parser.next();
    while (-1 !== option) {
        switch (option) {
        case "h":
        case "p":
        case "v":
            opt[option] = true;
            break;
        case "g":
            opt[option].push(parser.optarg);
            break;
        case "o":
            if (!parseOption(opt[option], parser.optarg)) {
                process.exit(64); // EX_USAGE
            }
            break;
        default:
            usage();
            process.exit(64); // EX_USAGE
        }
        option = parser.next();
    }

    // We always need "fudge" so column and line numbers agree with JSHint.
    opt.o.fudge = true;

    // The remaining arguments are source files.
    const sourceFiles = argv.slice(parser.optind);

    if (opt.h) {
        usage();
        return;
    }

    if (opt.v) {
        console.log(jslint("").edition);
        return;
    }

    /**
     * Print a report in the style of JSHint. Return the number of warnings.
     */
    function printReport([path, report]) {
        if (
            opt.p ||
            hasWarning(report, "bad_property_a") ||
            hasWarning(report, "unregistered_property_a")
        ) {
            const property_text = jslintReport.property(report);
            if (property_text) {
                console.log(property_text);
            }
        }

        report.warnings.forEach(function (warning) {
            console.error(
                `${path}: ` +
                `line ${1 + warning.line}, ` +
                `col ${1 + warning.column}, ` +
                warning.message
            );
        });

        if (report.ok) {
            console.log(`${path}: is OK.`);
        } else if (report.stop) {
            console.error(`${path}: JSLint was unable to finish.`);
        } else {
            console.error(`${path}: ${report.warnings.length} warnings.`);
        }

        return Number(report.stop) + report.warnings.length;
    }

    /**
     * Create a requestor that prints a report for the file.
     */
    const loadFileAndPrintReport = (file) => parseq.sequence([
        constant(file),
        parseq.parallel([
            realpath,
            parseq.sequence([
                readFile,
                requestorize((source) => jslint(source, opt.o, opt.g))
            ])
        ]),
        requestorize(printReport)
    ]);

    // A requestor that runs jslint on all the source files.
    const printedReports = parseq.sequence([
        (
            0 !== sourceFiles.length
            ? parseq.parallel(sourceFiles.map(loadFileAndPrintReport))
            : constant([])
        ),
        requestorize(sum)
    ]);

    printedReports(function (warnings, failure) {
        if (failure) {
            console.error(String(failure));
            process.exit(2);
        }
        if (0 < warnings) {
            console.error(`${warnings} total warnings.`);
            process.exit(1);
        }
    });
}
main(process.argv);

export default Object.freeze(main);
