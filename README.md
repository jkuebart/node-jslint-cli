JSLint command-line interface
=============================

This package provides a command-line interface for [JSLint][JL].

The command `jslint` runs JSLint on each file given on the command line and
outputs the list of warnings produced, if any. The format of the warnings
is compatible with [JSHint][JH]. Consequently, existing problem matchers
for JSHint will work without changes.

This package differs from existing offerings in that
  * its major version number is equal to the JSLint »edition«
  * it is automatically updated when a new edition of JSLint is released
  * it contains unaltered JSLint source code
  * the code is itself JSLint-clean
  * it prints property directives automatically when they are incomplete
  * the output is compatible to JSHint
  * it can be used as a module inside another application (both a CommonJS
    module and an ECMAScript 6 module are provided)
  * it uses Douglas Crockford's [parseq][RQ] for parallelism
  * there is no support for a configuration file


Versioning
----------

The major version number of this package indicates the JSLint »edition« it
contains. Multiple updates to JSLint on a single day are represented by
increasing minor version numbers. The patch version indicates updates to
this package itself.


Synopsis
--------

    jslint [-hpv][-g <global>][-o <option>[=<value>]] [files...]

Run JSLint on the given *files* and print the warnings, if any.


Options
-------

<dl>
  <dt><code>-g &lt;global></code></dt>
  <dd>
    The name of a global variable that the file is allowed readonly access.
    Can be used multiple times. Also see <code>-o</code> for options which
    automatically define global variables for well-known environments such as
    browser or Node.js.
  </dd>

  <dt><code>-h</code></dt>
  <dd>
    Print usage instructions and exit.
  </dd>

  <dt><code>-o &lt;option>[=&lt;value>]</code></dt>
  <dd>
    Pass an option to JSLint. Check the <a href="http://jslint.com/help.html"
    >JSLint documentation</a> for the full list of options. The option is set
    to <code>true</code> if no <code>value</code> is given, otherwise it should
    be a valid JSON string.
  </dd>

  <dt><code>-p</code></dt>
  <dd>
    Print a <code>/*property</code> directive which can be pasted into the
    source file. By default, the directive is only printed if there are
    warnings about unregistered properties.
  </dd>

  <dt><code>-v</code></dt>
  <dd>
    Print the »edition« of the bundled JSLint and exit.
  </dd>
</dl>


Exit status
-----------

`jslint` returns 0 if no warnings were reported, 1 if there was at least
one warning or 2 for other errors. In the event of incorrect usage, the
exit status is `EX_USAGE` (64).


Example
-------

    $ jslint src/js/main.js
    /src/jslint-cli/src/js/main.js: line 75, col 1, This function needs a "use strict" pragma.
    /src/jslint-cli/src/js/main.js: line 101, col 1, Expected 'while' to be in a function.
    /src/jslint-cli/src/js/main.js: 2 warnings.


API usage
=========

This module can also be used from Node.js code. It is installed using

    $ npm install jslint-cli

The module exports a single function which expects an array of command-line
arguments as described above. See [this module][NJL] if you'd like to use
JSLint itself as a Node.js module.

```javascript
const jslintCli = require("jslint-cli");

jslintCli([
    "-o",
    "browser",
    "package.json",
    "some/file.js"
]);
```


Updating
========

A shell script is provided for building new packages when this project or
the upstream @jkuebart/jslint is updated. It can be run using

    npm run editions

This creates branches and tags based on the »edition« of the upstream
project. Packages still need to be generated and published manually.

The local branches and tags can be viewed using

    npm run show-branches
    npm run show-tags

This can be used to automate some tasks, for example:

    npm run show-branches --silent |
    while read b
    do
        git push --set-upstream origin "${b#refs/heads/}:${b#refs/heads/}"
    done

or

    npm run show-tags --silent |
    while read t
    do
        git checkout "${t#refs/tags/}"
        npm install
        npm publish --access public
    done

To easily remove automatically created local branches and tags, use

    npm run reset

There is also a shell script that determines whether the upstream project
has been updated.

    npm run show-branches --silent |
    npm run uptodate --silent


[JH]: http://jshint.com/
[JL]: http://jslint.com/
[NJL]: http://gitlab.com/jkuebart/node-jslint/
[RQ]: http://github.com/douglascrockford/parseq/
