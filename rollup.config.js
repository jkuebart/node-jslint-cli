/*property
    banner, exports, external, file, format, freeze, input, main, module,
    output, plugins, sourcemap
*/

import pkg from "./package.json";

import commonjs from "@rollup/plugin-commonjs";
import nodeResolve from "@rollup/plugin-node-resolve";

export default Object.freeze({
    external: [
        "console",
        "fs",
        "process"
    ],
    input: "js/jslint-cli.js",
    output: [
        {
            file: pkg.module,
            format: "es",
            sourcemap: true
        },
        {
            banner: "#!/usr/bin/env node",
            exports: "default",
            file: pkg.main,
            format: "cjs",
            sourcemap: true
        }
    ],
    plugins: [
        commonjs(),
        nodeResolve()
    ]
});
